1.buat Database


create Database myshop;

 

2. Membuat table users, categories, dan items

//table users
create table users (
    -> id int auto_increment primary key,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255));


//table categories
create table categories (
    -> id int auto_increment primary key,
    -> name varchar(255));


//table items
create table items (
    -> id int auto_increment primary key,
    -> name varchar(255),
    -> description varchar(255),
    -> price int,
    -> stock int,
    -> category_id int,
    -> foreign key(category_id) references categories(id));




3. Memasukkan data pada table

//users
insert into users(name, email, password) values(
    -> "John Doe", "john@doe.com", "john123"),
    -> ("Jane Doe", "jane@doe.com", "jane123");



//categories
insert into categories(name) values(
    -> "gadget"), ("cloth"), ("men"), ("women"), ("branded");



//items
insert into items(name, description, price, stock, category_id) values(
    -> "Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1),
    -> ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2),
    -> ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);




4.

  a. select id, name, email from users; //semua field kecuali password
  
  b. select * from items
    -> where price > 1000000; //item yang memiliki harga di atas 1000000(satu juta)
     
     select * from
    -> items where name like '%watch'; //item yang memiliki name serupa atau mirip (like)

  c. select items.id, items.name, items.description, items.price, items.stock, items.category_id, categories.name as kategori from items inner join categories on items.category_id = categories.id;


5.

update items set price = 2500000 where id = 1; //update data table